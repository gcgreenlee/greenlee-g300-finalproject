using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leaderMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed = 2f;


    // Update is called once per frame
    void Update()
    {
        float currentX = rb.transform.position.x;
        float currentY = rb.transform.position.y;
        // Debug.Log(currentX);
        // Debug.Log(currentY);

        if (currentY >= 40f)
        {
            rb.velocity = Vector2.zero;
        }
        else
        {
            rb.velocity = Vector2.up * speed * Time.deltaTime;

        }
    }
}
