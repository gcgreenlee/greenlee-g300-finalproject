using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Threading;


public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;
    public Animator animator;
    float horizontalMove = 0f;
    public float runSpeed = 60f;
    bool jump = false;
    public Renderer playerRenderer;
    public bool isPlayerVisible = true;
    public GameEnding gameEnding;
    public TextMeshProUGUI countText;
    bool timerOn = false;
    public float currentTime = 3f;
    public Image blackBox;
    public TextMeshProUGUI instructionText;
    int pickupCounter = 0;
    public TextMeshProUGUI pickups;
    public AudioSource pickupDing;
    public AudioSource backgroundMusic;
    public TextMeshProUGUI ending;



    void Start()
    {
        instructionText.text = "Welcome to my Final Project!" + "\n\n" + "Use WASD to move the character around" + "\n\n" + "Use the space bar to jump" + "\n\n" + "Keep up with the screen, if you are out for more than 3 seconds you die!" + "\n\n" + "Try to collect the diamonds to increase your score" + "\n\n" + "Make your way to the flag to win!" + "\n\n" + "Press any key to begin!";
        blackBox.enabled = true;
        Time.timeScale = 0;
        playerRenderer = GetComponent<Renderer>();
        backgroundMusic.Play();
    }

    // Update is called once per frame
    void Update()
    {

        pickups.text = "Diamonds: " + string.Format("{0}", pickupCounter);

        if(Input.anyKeyDown)
        {
            Time.timeScale = 1;
            instructionText.text = "";
            blackBox.enabled = false;
        }

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (isPlayerVisible == false)
        {
            timerOn = true;
        }
        else
        {
            timerOn = false;
            countText.text = "";
            currentTime = 3f;
        }

        if (timerOn)
        {
            if (currentTime > 0)
            {
                Timer(currentTime);
            }
            else
            {
                currentTime = 0;
                timerOn = false;
                GameEnding(false);
            }
        }

        animator.SetFloat("speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonDown("Jump")) 
        {
            jump = true;
            animator.SetBool("isjumping", true);
        }

        if (playerRenderer.isVisible == false)
        {
            isPlayerVisible = false;
        }
        else 
        {
            isPlayerVisible = true;
        }
    }

    void Timer(float time) 
    {
        countText.text = time.ToString("0");
        currentTime -= 1 * Time.deltaTime;
    }

    public void OnLanding () 
    {
        animator.SetBool("isjumping", false);
    }

    void FixedUpdate() 
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Mud")
        {
            runSpeed = 20f;
        }
        if (other.gameObject.tag == "Ground")
        {
            runSpeed = 60f;
        }
        if (other.gameObject.tag == "GameEnding")
        {
            GameEnding(true);
        }
        if (other.gameObject.tag == "Pickup")
        {
            pickupCounter += 1;
            other.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            other.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            pickupDing.Play();
        }
    }

    void GameEnding(bool win)
    {
        backgroundMusic.Stop();
        blackBox.enabled = true;
        if (win)
        {
            ending.text = "You won!" + "\n\n" + "You collected " + string.Format("{0}", pickupCounter) + " out of 8 diamonds!" + "\n\n" + "Press any key to quit";
            if (Input.anyKeyDown)
            {
                Application.Quit();
            }             
        }
        else
        {
            ending.text = "You died!" + "\n\n" + "Press any key to restart...";
            if (Input.anyKeyDown)
            {
                SceneManager.LoadScene(0);
            }            
        }
    }
}
